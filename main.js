var pos = [
    [[0,0],[0,1],[0,2]], 
    [[1,0],[1,1],[1,2]], 
    [[2,0],[2,1],[2,2]], 
    [[0,0],[1,0],[2,0]], 
    [[0,1],[1,1],[2,1]], 
    [[0,2],[1,2],[2,2]], 
    [[0,0],[1,1],[2,2]], 
];
function proverka() {
    var table = document.getElementById("area");

    var flagP = false;
    var flagC = false;
    for(var i = 0; i < pos.length; i++){
        var win = true;
        for(var k = 0; k < pos[i].length; k++){
            if(table.rows[pos[i][k][0]].cells[pos[i][k][1]].innerHTML !== 'X') 
                win = false;
        }
        if(win){
            flagP = true;
            break;
        }
    }  

    for(var i = 0; i < pos.length; i++){
        var lose = true;
        for(var k = 0; k < pos[i].length; k++){
            if(table.rows[pos[i][k][0]].cells[pos[i][k][1]].innerHTML !== 'O') 
            lose = false;
        }
        if(lose){
            flagC = true;
            break;
        }
    }  

    if (flagP) {
      return 'Pwin';
    }
    if (flagC) {
        return 'Cwin';
      }
    return '';
}

var tds = document.getElementsByTagName('td');
for(var i = 0; i < tds.length; i++){
    tds[i].addEventListener('click', function(){
        if(this.innerHTML !== 'X' && this.innerHTML !== 'O'){
            this.innerHTML = 'X';
            stepComp(tds);
            if(proverka() == 'Pwin')
            {
                alert("победа");
                clearArea();
            }
            else if(proverka() == 'Cwin')
            {
                alert("поражение");
                clearArea();
            }
        } 
    })
}

  function stepComp(tds) {
    for (var i = 0; i < tds.length; i++) {
      if (tds[i].innerHTML == '') {
        tds[i].innerHTML = 'O';
        break;
      }
    }
    proverka();
  }
  
 function clearArea(){
    var alles_td=document.getElementsByTagName("td");
        for(i=0;i<alles_td.length;i++){
            alles_td[i].innerHTML="";
        }
 }
  
 